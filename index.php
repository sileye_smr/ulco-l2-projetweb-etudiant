<?php

/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');

// GET "/store"
$router->get('/store', 'controller\StoreController@store');

// GET "/store/{:num}"
$router->get('/store/{:num}', 'controller\StoreController@product');



// GET "/account"


$router->get('/account', 'controller\AccountController@account');
$router->post("/login", "Controller@callback");
$router->post("/signin", "Controller@callback");

//
$router->post('/account/login', 'controller\AccountController@login');
$router->post('/account/signin', 'controller\AccountController@signin');

//GET "/account/logout"
$router->get('/account/logout','controller\AccountController@logout');
//GET "/account/deleteAccount
$router->get('/account/deleteAccount','controller\AccountController@deleteAccount');



//POST "/postComment
$router->post('/postComment/{:num}','controller\CommentController@postComment');

//POST "/search"
$router->post('/store/search','controller\StoreController@search');

//GET "/account/infos"
$router->get('/account/infos','controller\AccountController@infos');
//POST "/account/update"
$router->post('/account/update','controller\AccountController@update');

//GET "/cart"
$router->get('/cart','controller\CartController@cart');

//POST "/cart/add"
$router->post('/cart/add','controller\CartController@add');

//GET "/cart/cleanPanier"
$router->get('/cart/cleanCart','controller\CartController@cleanCart');

//GET "/cart/cleanPanier"
$router->get('/cart/removeProductCart/{:num}','controller\CartController@removeProductCart');


// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
