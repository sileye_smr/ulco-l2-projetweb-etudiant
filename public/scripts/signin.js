document.addEventListener('DOMContentLoaded',function(){
    let firstname=document.getElementById('userfirstname')
    let lastname=document.getElementById('userlastname')
    let mail=document.getElementById('useremail')
    let password=document.getElementById('userpass')
    let passwordconf=document.getElementById('userpassconf')
    let formSignin=document.getElementById('formSignin')
    firstname.addEventListener('keyup',function(){
        if(/^\w{2,}$/.test(firstname.value)) {
            firstname.setAttribute('class','valid')
            firstname.previousElementSibling.setAttribute('class','valid')
        }
        else{
            firstname.setAttribute('class','invalid')
            firstname.previousElementSibling.setAttribute('class','invalid')
        }

    })

    lastname.addEventListener('keyup',function(){
        if(/^\w{2,}$/.test(lastname.value)) {
            lastname.setAttribute('class','valid')
            lastname.previousElementSibling.setAttribute('class','valid')
        }
        else {
            lastname.setAttribute('class','invalid')
            lastname.previousElementSibling.setAttribute('class','invalid')
        }
    })

    mail.addEventListener('keyup',function(){
        if(/^([a-z0-9._-]+)@([a-z0-9._-]+)\.([a-z]{2,6})$/.test(mail.value)) {
            mail.setAttribute('class','valid')
            mail.previousElementSibling.setAttribute('class','valid')
        }
        else {
            mail.setAttribute('class','invalid')
            mail.previousElementSibling.setAttribute('class','invalid')
        }
    })

    password.addEventListener('keyup',function(){
        if(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]){6,}$/.test(password.value)){
            password.setAttribute('class','valid')
            password.previousElementSibling.setAttribute('class','valid')
        }
        else {
            password.setAttribute('class','invalid')
            password.previousElementSibling.setAttribute('class','invalid')
        }
    })

    passwordconf.addEventListener('keyup',function(){
        if(password.value===passwordconf.value) {
            passwordconf.setAttribute('class','valid')
            passwordconf.previousElementSibling.setAttribute('class','valid')
        }
        else {
            passwordconf.setAttribute('class','invalid')
            passwordconf.previousElementSibling.setAttribute('class','invalid')
        }

    })

    formSignin.addEventListener('submit',function(event){

        event.preventDefault();
        let Inputinvalid=document.getElementsByClassName('invalid')
        if(Inputinvalid.length===0) formSignin.submit();

    })
})
