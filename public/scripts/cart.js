document.addEventListener('DOMContentLoaded',function(){
    let moins=document.getElementsByClassName('moins')
    let plus=document.getElementsByClassName('plus')
    let total=document.getElementById('total')
    const QuantiteMax=5
    //Evenement pour augmenter la quantite du produit
    for(let btnPlus of plus){
        btnPlus.addEventListener('click',function(){
            let quantite=btnPlus.previousElementSibling
            let nbQuantite=parseInt(quantite.textContent)
            if(nbQuantite<QuantiteMax){
                nbQuantite+=1
                quantite.textContent=String(nbQuantite)
                total.textContent=String(calculPrix())
                if(nbQuantite===QuantiteMax) btnPlus.nextElementSibling.style.visibility='visible'
            }
        })
    }


    //Evenement pour diminuer la quantite du produit
    for(let btnMoins of moins){
        btnMoins.addEventListener('click',function(){
            let quantite=btnMoins.nextElementSibling
            let nbQuantite=parseInt(quantite.textContent)
            if(nbQuantite>1){
                nbQuantite-=1
                quantite.textContent=String(nbQuantite)
                total.textContent=String(calculPrix())
                if(nbQuantite===QuantiteMax-1) btnMoins.nextElementSibling.nextElementSibling.nextElementSibling.style.visibility='hidden'
            }
        })
    }

    function calculPrix(){
        let prixTotal=0
        for(let elem of moins){
            let prixUnite=parseInt(elem.previousElementSibling.textContent)
            let nbQuantite=parseInt(elem.nextElementSibling.textContent)
            prixTotal+=prixUnite*nbQuantite
        }
        return prixTotal
    }
    total.textContent=String(calculPrix())
})
