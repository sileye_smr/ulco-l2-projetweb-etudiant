<div id="store">

<!-- Filtrer l'affichage des produits  ---------------------------------------->

<form  method="POST" action="/store/search">

  <h4>Rechercher</h4>
  <input type="text" name="search" placeholder="Rechercher un produit" autocomplete="off" />

  <h4>Catégorie</h4>
  <?php foreach ($params["categories"] as $c) { ?>
    <input type="checkbox" name="category[]" value="<?= $c["name"] ?>" />
    <?= $c["name"] ?>
    <br/>
  <?php } ?>

  <h4>Prix</h4>
    <input type="radio" name="order" /> Croissant <br />
    <input type="radio" name="order" /> Décroissant <br />
    <input type="radio" name="order" value="ASC"/> Croissant <br />
    <input type="radio" name="order" value="DESC"/> Décroissant <br />


  <div><input type="submit" value="Appliquer" /></div>

</form>

<!-- Affichage des produits --------------------------------------------------->

<div class="products">

<?php foreach ($params["list"] as $product) { ?>
  <div class="card">
    <p class="card-image">
      <img src="/public/images/<?= $product["image"] ?>" />
    </p>
    <p class="card-category">
      <?= $product["category"] ?>
    </p>
    <p class="card-title">
      <a href="/store/<?= $product["id"] ?>">
        <?= $product["name"] ?>
      </a>
    </p>
    <p class="card-price"><?= $product["price"] ?>€</p>
  </div>
    <?php if($params['searchProduct']=='ProductNotFound'):?>
        <?php if(isset($params['searchProduct']) AND $params['searchProduct']=='ProductNotFound'):?>
        <div class="card"><h2>Le produit que vous recherchez n'existe pas ou ne commence pas par le mot que vous avez saisie</h2></div>
    <?php endif;?>
    <?php endif;?>

<?php } ?>

</div>

</div>
