<div id="product">

<div>

  <div class="product-images">
    <img id="splash" src="/public/images/<?= $params["product"]["image"] ?>" />
    <div class="product-miniatures">
      <div>
        <img src="/public/images/<?= $params["product"]["image"] ?>" />
      </div>
      <div>
        <img src="/public/images/<?= $params["product"]["image_alt1"] ?>" />
      </div>
      <div>
        <img src="/public/images/<?= $params["product"]["image_alt2"] ?>" />
      </div>
      <div>
        <img src="/public/images/<?= $params["product"]["image_alt3"] ?>" />
      </div>
    </div>
  </div>

  <div class="product-infos">
    <p class="product-category">
      <?= $params["product"]["category"] ?>
    </p>
    <h1><?= $params["product"]["name"] ?></h1>
    <p class="product-price">
      <?= $params["product"]["price"] ?>€
    </p>
    <form>
        <form method="post" action="/cart/add">
      <button id="btn_minus" type="button">-</button>
      <button id="btn_quantity" type="button">1</button>
      <button id="btn_plus" type="button">+</button>
            <input type="hidden" name="id" value="<?=$params['produit']['prodId']?>"/>
            <input type="hidden" name="image" value="<?=$params['produit']['image']?>"/>
            <input type="hidden" name="categorie" value="<?=$params['produit']['catName']?>"/>
            <input type="hidden" name="nom" value="<?=$params['produit']['prodName']?>"/>
            <input type="hidden" name="prix" value="<?=$params['produit']['price']?>"/>
            <input type="hidden" name="quantite" id="quantiteHidden" value=""/>

            <input type="submit" value="Ajouter au panier" />
    </form>
    <p id="box_quantity" class="box error">
      Quantité maximale autorisée !
    </p>
  </div>

</div>

<div>
  <div class="product-spec">
    <h2>Spécificités</h2>
    <?= $params["product"]["spec"] ?>  
  </div>
  <div class="product-comments">
    <!-- TODO: Gérer les commentaires -->
    <h2>Avis</h2>
    <p>
      Il n'y a pas d'avis sur ce produit.
    </p>
      <?php if($params['comment']):?>
      <?php if(!empty($params['comment'])):?>
          <?php foreach($params['comment'] as $comment):?>
              <div class="product-comment">
                  <p class="product-comment-author"><?=$comment['firstname']." ".$comment['lastname']?></p>
                  <p>
                      <?=$comment['content']?><br/><br/>
                      Date : <?=$comment['date']?>
                  </p>

              </div>
          <?php endforeach;?>
      <?php else:?>
          <p>Il n'y a pas d'avis pour ce produit.</p>
      <?php endif;?>
      <?php if(isset($_SESSION['usermail'])):?>
          <form  method="post" id="postComment" action="/postComment/<?=$params['produit']['prodId']?>">
              <p><input type="text" name="content" placeholder="Rediger un commentaire" id="contentPost"/></p>
              <p><input type="submit" value="Publier" /></p>
          </form>
      <?php endif;?>
      <?php endif;?>
  </div>
</div>
</div>

<script src="/public/scripts/product.js"></script>