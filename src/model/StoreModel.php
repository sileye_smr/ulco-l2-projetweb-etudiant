<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";

    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function listProducts(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT c.name as category, p.id, p.name, p.price, p.image
            FROM product AS p
            INNER JOIN category AS c ON p.category = c.id";

    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function infoProduct(int $id)
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT c.name as category, p.id, p.name, p.price, p.image,
                   p.image_alt1, p.image_alt2, p.image_alt3, p.spec
            FROM product AS p
            INNER JOIN category AS c ON p.category = c.id
            WHERE p.id = ?";

    // Exécution de la requête
    $sth = $db->prepare($sql);
    $sth->execute(array($id));

    // Retourner les résultats (type array)
    return $sth->fetch();
  }

    //metethode qui recupere la liste de produit selon les criteres de recherches
    static  function searchListProducts(array $dataSearch):array{
        $db=\model\Model::connect();
        if($dataSearch['search']!=null AND $dataSearch['category']==null AND $dataSearch['order']==null){
            $sql="SELECT product.id as prodId,product.name as prodName,price,image,category.name as catName FROM product INNER JOIN category ON product.category=category.id WHERE product.name LIKE (?)";
            $req=$db->prepare($sql);
            $req->execute(array($dataSearch['search']."%"));
            if(!empty($req))return $req->fetchAll();
            return array();
        }elseif($dataSearch['search']==null AND $dataSearch['category']!=null AND $dataSearch['order']==null){
            $sql="SELECT product.id as prodId,product.name as prodName,price,image,category.name as catName FROM product INNER JOIN category ON product.category=category.id WHERE category.name IN(?,?,?)";
            $req=$db->prepare($sql);
            $a=(!empty($dataSearch['category'][0])) ? $dataSearch['category'][0] : '';
            $b=(!empty($dataSearch['category'][1])) ? $dataSearch['category'][1] : '';
            $c=(!empty($dataSearch['category'][2])) ? $dataSearch['category'][2] : '';
            $req->execute(array($a,$b,$c));
            return $req->fetchAll();
        }elseif($dataSearch['search']==null AND $dataSearch['category']==null AND $dataSearch['order']!=null){
            if($dataSearch['order']=='ASC') $sql="SELECT product.id as prodId,product.name as prodName,price,image,category.name as catName FROM product INNER JOIN category ON product.category=category.id ORDER BY price ASC";
            else $sql="SELECT product.id as prodId,product.name as prodName,price,image,category.name as catName FROM product INNER JOIN category ON product.category=category.id ORDER BY price DESC";
            $req=$db->prepare($sql);
            $req->execute();
            return $req->fetchAll();
        }elseif($dataSearch['search']!=null AND $dataSearch['category']!=null AND $dataSearch['order']==null){
            $sql="SELECT product.id as prodId,product.name as prodName,price,image,category.name as catName FROM product INNER JOIN category ON product.category=category.id WHERE product.name LIKE (?) AND category.name=?";
            $req=$db->prepare($sql);
            $a=(!empty($dataSearch['category'][0])) ? $dataSearch['category'][0] : '';
            $b=(!empty($dataSearch['category'][1])) ? $dataSearch['category'][1] : '';
            $c=(!empty($dataSearch['category'][2])) ? $dataSearch['category'][2] : '';
            $req->execute(array($dataSearch['search']."%",$a,$b,$c));
            return $req->fetchAll();
        }elseif($dataSearch['search']==null AND $dataSearch['category']!=null AND $dataSearch['order']!=null){
            if($dataSearch['order']=='ASC') $sql="SELECT product.id as prodId,product.name as prodName,price,image,category.name as catName FROM product INNER JOIN category ON product.category=category.id WHERE category.name IN (?,?,?) ORDER BY price ASC";
            else $sql="SELECT product.id as prodId,product.name as prodName,price,image,category.name as catName FROM product INNER JOIN category ON product.category=category.id WHERE category.name IN (?,?,?) ORDER BY price DESC";
            $req=$db->prepare($sql);
            $a=(!empty($dataSearch['category'][0])) ? $dataSearch['category'][0] : '';
            $b=(!empty($dataSearch['category'][1])) ? $dataSearch['category'][1] : '';
            $c=(!empty($dataSearch['category'][2])) ? $dataSearch['category'][2] : '';
            $req->execute(array($a,$b,$c));
            return $req->fetchAll();
        }elseif($dataSearch['search']!=null AND $dataSearch['category']==null AND $dataSearch['order']!=null){
            if($dataSearch['order']=='ASC') $sql="SELECT product.id as prodId,product.name as prodName,price,image,category.name as catName FROM product INNER JOIN category ON product.category=category.id WHERE product.name LIKE (?) ORDER BY ASC";
            else $sql="SELECT product.id as prodId,product.name as prodName,price,image,category.name as catName FROM product INNER JOIN category ON product.category=category.id WHERE product.name LIKE (?)ORDER BY DESC";
            $req=$db->prepare($sql);
            $req->execute(array($dataSearch['search']."%"));
            return $req->fetchAll();
        }elseif($dataSearch['search']!=null AND $dataSearch['category']!=null AND $dataSearch['order']!=null){
            if($dataSearch['order']=='ASC') $sql="SELECT product.id as prodId,product.name as prodName,price,image,category.name as catName FROM product INNER JOIN category ON product.category=category.id WHERE product.name LIKE (?) AND category.name IN (?,?,?) ORDER BY price ASC";
            else $sql="SELECT product.id as prodId,product.name as prodName,price,image,category.name as catName FROM product INNER JOIN category ON product.category=category.id WHERE product.name LIKE (?) AND category.name IN (?,?,?) ORDER BY price DESC";
            $req=$db->prepare($sql);
            $a=(!empty($dataSearch['category'][0])) ? $dataSearch['category'][0] : '';
            $b=(!empty($dataSearch['category'][1])) ? $dataSearch['category'][1] : '';
            $c=(!empty($dataSearch['category'][2])) ? $dataSearch['category'][2] : '';
            $req->execute(array($a,$b,$c,$dataSearch['search']."%"));
            return $req->fetchAll();
        }
        return self::listProducts();
    }

}