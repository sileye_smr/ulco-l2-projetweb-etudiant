<?php

namespace controller;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $list = \model\StoreModel::listProducts();

    // Variables transmises à la vue
    $params = array(
      "module" => "store.php",
      "title" => "Store",
      "categories" => $categories,
      "list" => $list
    );

    // Faire le rendu de la vue "src/view/template.php"
    \view\Template::render($params);
  }

  public function product(int $id): void
  {
    // Communications avec la base de données
    $product = \model\StoreModel::infoProduct($id);

    // Si le produit n'existe pas
    if ($product == null) {
      header("Location: /store");
      exit();
    }

    // Variables transmises à la vue
    $params = [
      "module"  => "product.php",
      "title"   => "Produit " . $id,
      "product" => $product
    ];

    // Faire le rendu de la vue "src/view/template.php"
    \view\Template::render($params);
  }
    //methode qui affiche les produit en fonction des criteres de recherches
    public function search()
    {
        $dataSearch = array();
        $dataSearch['search'] = (!empty($_POST['search'])) ? htmlspecialchars($_POST['search']) : null;
        $dataSearch['category'] = (!empty($_POST['category'])) ? $_POST['category'] : null;
        $dataSearch['order'] = (!empty($_POST['order'])) ? htmlspecialchars($_POST['order']) : null;

        // Communications avec la base de données
        $categories = \model\StoreModel::listCategories();
        $produits = \model\StoreModel::searchListProducts($dataSearch);
        $searchProduct = (empty($produits)) ? "ProductNotFound" : "ProductFound";
        // Variables à transmettre à la vue
        $params = array(
            "title" => "Store",
            "module" => "store.php",
            "categories" => $categories,
            "produits" => $produits,
            "searchProduct" => $searchProduct
        );
        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);

    }


}