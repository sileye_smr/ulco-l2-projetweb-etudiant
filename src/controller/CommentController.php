<?php


namespace controller;


class CommentController
{
    public function postComment(int $id_product){
        $content=htmlspecialchars($_POST['content']);
        $mail=(isset($_SESSION['usermail']))?$_SESSION['usermail']:'empty';
        if(\model\CommentModel::insertComment($id_product,$content,$mail)){
            header('location:/store/'.$id_product.'?statusComment=post_succes');
            exit();
        }
        header('location:/store/'.$id_product.'?statusComment=post_fail');
        exit();
    }
}
